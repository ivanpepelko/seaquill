<?php

/**
 * @author Ivan Pepelko
 */
//config
require_once dirname(__FILE__) . '/dep/src/PHPSQLParser.php';

switch (PHP_OS) {
    case 'Linux':
        define('UPDATES_FILE', '/var/www/klikured-cloud/sql/updates/updates_43_b.sql');
        define('ENTITY_PATH', '/var/www/klikured-cloud/src/Orion/KlikUredCustomBundle/Entity/');
        define('FORMTYPE_PATH', '/var/www/klikured-cloud/src/Orion/KlikUredBundle/Form/Type/');
        break;
    case 'WINNT':
        define('UPDATES_FILE', 'D:\wamp\www\klikured\sql\updates\update_2014_10_14_00.sql');
        define('ENTITY_PATH', 'D:\wamp\www\klikured\src\Orion\KlikUredCustomBundle\Entity');
        define('FORMTYPE_PATH', 'D:\wamp\www\klikured\src\Orion\KlikUredBundle\Form\Type');
        break;
}

//action
class Seaquill {

    public $entity;
    public $repository;
    public $formtype;
    public $table_name;
    public $class_name;
    public $definition;
    public $columns = array();
    public $types = array(
        'INT' => 'integer',
        'DECIMAL' => 'decimal',
        'TEXT' => 'string',
        'CHAR' => 'string',
        'VARCHAR' => 'string',
        'DATE' => 'date',
        'DATETIME' => 'datetime',
        'BIGINT' => 'integer',
        'MEDIUMTEXT' => 'string',
        'TIMESTAMP' => 'datetime',
        'BLOB' => 'boolean', //KEMIJA ZA TINYINT
    );
    public $types_form = array(
        'string' => 'text',
        'integer' => 'number',
        'decimal' => 'number',
        'datetime' => 'datetime',
        'date' => 'date',
        'boolean' => 'checkbox'
    );

    public function __construct($lines, $sql_file = UPDATES_FILE, $entity_path = ENTITY_PATH, $formtype_path = FORMTYPE_PATH) {

        $lines = explode('-', $lines);

        $file = file($sql_file);

        $query = '';

        for ($i = $lines[0] - 1; $i < $lines[1] - 1; $i++) {
            $query .= $file[$i];
        }

        $query = str_replace('TINYINT', 'BLOB', $query);

        $parser = new PHPSQLParser($query, false);

        $table = $parser->parsed['TABLE'];

        $this->table_name = $table['no_quotes'];
        $this->class_name = self::camelize($table['no_quotes']);
        $this->definition = $table['create-def']['sub_tree'];

        $this->columns = $this->parseColumns($this->definition);

        $this->entity = $this->entityCreate();
        $this->repository = $this->repositoryCreate();
        $this->formtype = $this->formtypeCreate();

        file_put_contents($entity_path . $this->class_name . '.php', $this->entity);
        file_put_contents($entity_path . $this->class_name . 'Repository.php', $this->repository);
        file_put_contents($formtype_path . $this->class_name . 'Type.php', $this->formtype);

        if (!error_get_last())
            echo "Done!\n";
    }

    public static function camelize($string) {

        $camel = str_replace('_', ' ', $string);
        $camel = ucwords($camel);
        $camel = str_replace(' ', '', $camel);

        return $camel;
    }

    public static function underscorize($string) {
        $string = lcfirst($string);
        $array = str_split($string);
        foreach ($array as &$letter) {
            if (ctype_upper($letter)) {
                $letter = "_" . strtolower($letter);
            }
        }
        return implode('', $array);
    }

    public function parseColumns($definition) {
        $columns = array();
        foreach ($definition as $row) {
            if ($row['expr_type'] != 'primary-key') {
                $sub = $row['sub_tree'];
                $columns[] = array(
                    'name' => $sub[0]['no_quotes'],
                    'type' => $this->types[$sub[1]['sub_tree'][0]['base_expr']]
                );
            }
        }


        return $columns;
    }

    public function getPrimary($definition) {
        $last = array_pop($definition);
        return $last['sub_tree'][2]['sub_tree'][0]['no_quotes'];
    }

    public function entityCreate() {
        $entity = "<?php\n\n" .
                "/**\n" .
                " * $this->class_name\n" .
                " */\n\n" .
                "namespace Orion\KlikUredCustomBundle\Entity;\n\n" .
                "use Orion\\KlikUredCustomBundle\\Entity\\EntityJsonEncoder;\n" .
                "use Doctrine\ORM\Mapping as ORM;\n\n" .
                "/**\n" .
                " * $this->class_name\n" .
                " *\n" .
                " * @ORM\\Table(name=\"$this->table_name\")\n" .
                " * @ORM\\Entity(repositoryClass=\"Orion\\KlikUredCustomBundle\\Entity\\" . $this->class_name . "Repository\")\n" .
                " */\n" .
                "class $this->class_name extends EntityJsonEncoder {\n\n";
        foreach ($this->columns as $column) {
            $entity.= "    /**\n" .
                    "     *\n" .
                    "     * @ORM\\Column(name=\"" . $column['name'] . "\", type=\"" . $column['type'] . "\")\n";
            if ($column['name'] == 'id') {
                $entity .= "     * @ORM\\Id\n" .
                        "     * @ORM\\GeneratedValue(strategy=\"IDENTITY\")\n";
            }
            $entity.= "     */ \n" .
                    "    private $" . $column['name'] . ";\n" .
                    "\n";
        }

        foreach ($this->columns as $column) {
            $entity .= "    public function get" . self::camelize($column['name']) . "() {\n" .
                    "        return \$this->" . $column['name'] . ";\n" .
                    "    }\n\n" .
                    "    public function set" . self::camelize($column['name']) . "($" . $column['name'] . ") {\n" .
                    '        $this->' . $column['name'] . ' = $' . $column['name'] . ";\n" .
                    '        return $this;' . PHP_EOL .
                    "    }\n\n";
        }

        $entity .= "}\n";
        return $entity;
    }

    public function repositoryCreate() {
        $repository = "<?php\n\n" .
                "/**\n" .
                " * " . $this->class_name . "Repository\n" .
                " */\n\n" .
                "namespace Orion\\KlikUredCustomBundle\\Entity;\n\n" .
                "use Orion\\KlikUredBundle\\Entity\\KlikUredBaseRepository;\n\n" .
                "/**\n" .
                " * " . $this->class_name . "Repository\n" .
                " */\n" .
                "class " . $this->class_name . "Repository extends KlikUredBaseRepository {\n\n" .
                "}\n";
        return $repository;
    }

    public function formtypeCreate() {
        $data = "<?php\n\n" .
                "namespace Orion\\KlikUredBundle\\Form\\Type;\n\n" .
                "use Symfony\\Component\\Form\\AbstractType;\n" .
                "use Symfony\\Component\\Form\\FormBuilderInterface;\n" .
                "use Symfony\\Component\\OptionsResolver\\OptionsResolverInterface;\n" .
                "use Doctrine\\ORM\\EntityRepository;\n\n" .
                "class " . $this->class_name . "Type extends AbstractType {\n\n" .
                "    private \$controller;\n" .
                "    private \$doctrine;\n" .
                "    private \$session;\n" .
                "    private \$em;\n\n" .
                "    public function __construct(\\Orion\\KlikUredBundle\\Controller\\KlikUredBaseController \$controller, \\Doctrine\\ORM\\EntityManager \$entityManager) {\n" .
                "        \$this->controller = \$controller;\n" .
                "        \$this->doctrine = \$this->controller->getDoctrine();\n" .
                "        \$this->session = \$this->controller->getRequest()->getSession();\n" .
                "        \$this->em = \$entityManager;\n" .
                "    }\n\n" .
                "    public function setDefaultOptions(OptionsResolverInterface \$resolver) {\n" .
                "        parent::setDefaultOptions(\$resolver);\n" .
                "        \$resolver->setDefaults(array(\n" .
                "            'data_class' => 'Orion\\KlikUredCustomBundle\\Entity\\" . $this->class_name . "',\n" .
                "        ));\n" .
                "    }\n\n" .
                "    public function buildForm(FormBuilderInterface \$builder, array \$options) {\n" .
                "        \$builder\n";
        foreach ($this->columns as $column) {

            $data .= "            ->add('" . $column['name'] . "', '" . $this->types_form[$column['type']] . "', array('required' => false))\n";
        }
        $data.="            ;\n" .
                "    }\n\n" .
                "    public function getName() {\n" .
                "        return '" . $this->table_name . "';\n" .
                "    }\n\n" .
                "}\n";

        return $data;
    }

}

if (!isset($argv[1]))
    die("Lines are not defined!\nType \"seaquill help\" for usage info.\n");

if ($argv[1] == 'help')
    die("\n"
            . "Usage: php seaquill lines [-t [namespace]]\n"
            . "    PARAMETER : DESCRIPTION\n"
            . "    lines     : number of lines in sql updates file, format: int from-int to\n"
            . "                example: php seaquill 1-17\n"
            . "    -t        : optional if FormType should be created\n"
            . "    namespace : optional if entity is not in KlikUredCustomBundle namespace (needed only if FormType is being created) note: not yet implemented\n");

$seaquill = new Seaquill($argv[1]);
