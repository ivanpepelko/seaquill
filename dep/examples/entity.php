<?php

require_once dirname(__FILE__) . '/../src/PHPSQLParser.php';

$sql = "CREATE TABLE `work_positions` (" .
        "`id` INT(11) NOT NULL AUTO_INCREMENT," .
        "`description` VARCHAR(50) NULL DEFAULT NULL," .
        "`degree_id` INT(13) NOT NULL," .
        "`points` DECIMAL(10,2) NOT NULL DEFAULT '0.00',". 
        "PRIMARY KEY (`id`)" .
        ");";

echo $sql . "\n";
$start = microtime(true);
$parser = new PHPSQLParser($sql, false);
$stop = microtime(true);
print_r($parser->parsed);
echo "parse time simplest query:" . ($stop - $start) . "\n";


